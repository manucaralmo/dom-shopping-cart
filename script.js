// Iteración 1
// Eliminar productos al hacer click en Eliminar
const deleteBtns = document.querySelectorAll('button')

deleteBtns.forEach((elem) => {
	elem.addEventListener('click', (event) => {
		event.target.parentElement.remove()
		showCount()
		showTotal()
	})
})

// Iteración 2
// Mostrar total de productos
const count = document.getElementById('count')
const showCount = () => count.innerHTML = document.querySelectorAll('li').length

// Iteración 3: BONUS
// Mostrar total de la compra
const total = document.getElementById('total')

const showTotal = () => {
	let suma = 0

	document.querySelectorAll('.precio').forEach(precio => {
		suma += Number(precio.innerHTML)
	})

	total.innerHTML = suma + '€'
}
showTotal()

// Iteración 4: BONUS
// Añadir un botón y al hacer click añada: "Fresas - 4.95€"
const addFresasBtn = document.getElementById('addFresas')
const myUl = document.querySelector('ul')

addFresasBtn.addEventListener('click', () => {
	let newElem = document.createElement('li')

	newElem.innerHTML = 'Fresas <span class="precio"> 4.95</span>€ <button>Eliminar</button>'
	
	myUl.appendChild(newElem)

	showCount()
	showTotal()
})
